const { Client, Events, GatewayIntentBits, } = require('discord.js');
const { token } = require('./config.json');
const { clientId } = require('./config.json');
const {clientSecret} = require('./config.json');
const {accessToken} = require('./config.json');
// Create a new client instance
const client = new Client({ intents: [GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent
] });
var SpotifyWebApi = require('spotify-web-api-node');

// credentials are optional
var spotifyApi = new SpotifyWebApi({
  clientId: clientId,
  clientSecret: clientSecret,
  redirectUri :'https://example.com/callback'
});
var scopes = ['playlist-modify-public', 'user-read-email'],
  state = 'WhatIsThis';
var authorizeURL = spotifyApi.createAuthorizeURL(scopes, state);
console.log(authorizeURL);
// When the client is ready, run this code (only once)
// We use 'c' for the event parameter to keep it separate from the already defined 'client'
client.once(Events.ClientReady, c => {
	console.log(`Ready! Logged in as ${c.user.tag}`);
});
client.on('messageCreate', async interaction =>{
    let message = interaction.content;
    if (message.includes("!token")){
        let code = message.split(" ")[1];
        spotifyApi.authorizationCodeGrant(code).then(
            function(data) {
              console.log('The token expires in ' + data.body['expires_in']);
              console.log('The access token is ' + data.body['access_token']);
              console.log('The refresh token is ' + data.body['refresh_token']);
          
              // Set the access token on the API object to use it in later calls
              spotifyApi.setAccessToken(data.body['access_token']);
              spotifyApi.setRefreshToken(data.body['refresh_token']);
            },
            function(err) {
              console.log('Something went wrong!', err);
            }
          );
    }
    if (message.includes("!auth")){
        interaction.reply(authorizeURL);
    }
    if (message.includes("spotify") && (message.includes("track"))){
        console.log(message);
        let trackIdNotSplitFully = message.split("/");
        let trackId = trackIdNotSplitFully[trackIdNotSplitFully.length-1].split("?")[0];
        console.log(trackId)
        spotifyApi.addTracksToPlaylist('5jjMgD15U2Jw4fve5uyiGo', ["spotify:track:"+trackId])
            .then(function(data) {
                console.log('Added tracks to playlist!');
            }, function(err) {
                console.log('Something went wrong!', err);
            });
    }
});
// Log in to Discord with your client's token
client.login(token);